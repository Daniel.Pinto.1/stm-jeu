using UnityEngine;

[CreateAssetMenu(fileName = "New TrashItemData", menuName = "Trash Item Data", order = 0)]
public class TrashItemData : ScriptableObject
{
    [Header("Item Properties")]

    public string itemName;
    public int difficultyLevel;
    public string correctBinColor;
    public int pointsValue;
    [TextArea]
    public string message;
    
}
