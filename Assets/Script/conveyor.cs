using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    public GameObject conveyorBelt;
    public float speed = 10.0f;
    public Transform[] stopPositions;
    public GameObject[] objectsToSpawn;
    public static List<GameObject> objectsOnConveyor = new List<GameObject>();
    private bool conveyorActive = true;
    

    void Update()
    {
        if (conveyorActive)
        {
            MoveObjectsOnConveyor();
            Vector2 offset = new Vector2(Time.time * speed, 0);
            conveyorBelt.GetComponentInChildren<Renderer>().material.mainTextureOffset = offset;
        }
        else
        {
            conveyorBelt.GetComponentInChildren<Renderer>().material.mainTextureOffset = Vector2.zero;
        }
    }

    public void MoveObjectsOnConveyor()
    {
        for (int i = 0; i < objectsOnConveyor.Count; i++)
        {
            if (i < stopPositions.Length && objectsOnConveyor[i] != null)
            {
                MoveObjectTowards(objectsOnConveyor[i], stopPositions[i].position);
            }
        }
    }

    public void AddObjectToConveyor(GameObject newObj)
    {
        newObj.GetComponent<BoxCollider>().isTrigger = true;
       
        objectsOnConveyor.Add(newObj);
    }

    public void RemoveObjectFromConveyor(GameObject obj)
    {
        objectsOnConveyor.Remove(obj);
    }

    public void ToggleConveyor(bool isActive)
    {
        conveyorActive = isActive;
    }

    public void ObjectPickedUp(GameObject pickedUpObject)
    {
        if (objectsOnConveyor.Contains(pickedUpObject))
        {
            objectsOnConveyor.Remove(pickedUpObject);
            RearrangeObjects();
        }
    }

    int FindNextFreePosition()
    {
        for (int i = 0; i < stopPositions.Length; i++)
        {
            if (!IsPositionOccupied(i))
            {
                return i;
            }
        }
        return -1; // Indicate that no free position was found
    }

    void MoveObjectTowards(GameObject obj, Vector3 target)
    {
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        if (rb != null)
        {
            float distanceToTarget = Vector3.Distance(rb.position, target);

            if (distanceToTarget < 0.1f)
            {
                rb.MovePosition(target);
            }
            else
            {
                Vector3 direction = (target - obj.transform.position).normalized;
                rb.MovePosition(rb.position + direction * speed * Time.deltaTime);
            }
        }
    }

    public bool IsPositionOccupied(int positionIndex)
    {
        foreach (GameObject obj in objectsOnConveyor)
        {
            if (Vector3.Distance(obj.transform.position, stopPositions[positionIndex].position) < 0.5f)
            {
                return true;
            }
        }
        return false;
    }

    void CheckAndSpawnNewObject()
    {
        Debug.Log("Vérification pour la génération d'un nouvel objet...");
        if (objectsOnConveyor.Count < stopPositions.Length && !IsPositionOccupied(stopPositions.Length - 1))
        {
            int randomIndex = Random.Range(0, objectsToSpawn.Length);
            if (randomIndex >= 0 && randomIndex < objectsToSpawn.Length)
            {
                Debug.Log("Génération d'un nouvel objet à la position : " + (stopPositions.Length - 1));
                GameObject newObj = Instantiate(objectsToSpawn[randomIndex], stopPositions[stopPositions.Length - 1].position, objectsToSpawn[randomIndex].transform.rotation);

                

                // Correction de la rotation pour la bouteille
                if (newObj.name.Contains("Bottle_Sebastien"))
                {
                    newObj.transform.rotation = Quaternion.Euler(-90, 0, 0);
                }

                AddObjectToConveyor(newObj);
            }
        }
    }

    public void ResetConveyor()
    {
        foreach (GameObject obj in objectsOnConveyor)
        {
            Destroy(obj);
        }
        objectsOnConveyor.Clear();
    }

    void RearrangeObjects()
    {
        for (int i = 0; i < objectsOnConveyor.Count && i < stopPositions.Length; i++)
        {
            if (objectsOnConveyor[i] != null)
            {
                MoveObjectTowards(objectsOnConveyor[i], stopPositions[i].position);
            }
        }

        CheckAndSpawnNewObject();
    }
}
