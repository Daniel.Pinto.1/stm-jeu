using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashItem : MonoBehaviour
{

    public TrashItemData itemData;
    
    
    private Rigidbody rb; // Pour g�rer la physique de l'objet
    private bool hasTouchSmt = false; // Pour savoir si l'objet � deja touch� le sol ou un mur
    private bool wasThrown = false;

    public static event Action<TrashItem> GoBackToHand;
    public static event Action DestroyObjectGameobject;

    



    private void OnCollisionEnter(Collision collision)
    {
        if (collision != null)
        {
            if (collision.gameObject.CompareTag("Bin") && !hasTouchSmt)
            {
                if(itemData.correctBinColor == collision.gameObject.GetComponent<Bin>().binColor)
                {
                    GameManager.GMinstance.UpdateScore(itemData.pointsValue);
                    GameManager.GMinstance.playSound(1);
                }
                else
                {
                    if(!GameManager.GMinstance.isInFailedItem(itemData))
                    {
                        GameManager.GMinstance.addFailedItem(itemData);
                    }
                    
                    GameManager.GMinstance.UpdateScore(-20);
                    GameManager.GMinstance.playSound(0);
                }
                DestroyTheObject();
                
            }else if(collision.gameObject.CompareTag("Player"))
            {
                //Do nothing
            }
            else
            {
                hasTouchSmt=true;
                StartCoroutine(GoToHand());
                
            }
            
            
        }
    }

    IEnumerator GoToHand()
    {
        yield return new WaitForSeconds(2);
        GoBackToHand(this);
        hasTouchSmt = false;
        
    }
    public void ChangeThrownStat()
    {
        wasThrown = !wasThrown;
    }

    public bool GetThrownStat()
    {
        return wasThrown;
    }

    private void DestroyTheObject()
    {
        DestroyObjectGameobject();
        Destroy(gameObject);

    }
}
