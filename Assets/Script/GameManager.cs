using ECM.Components;
using ECM.Controllers;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Instance
    public static GameManager GMinstance;
    public GameObject startScreen; // Référence à l'écran de démarrage
    
    private PickupAndThrow pickupScript;
    public GameObject player;

    [SerializeField] private Conveyor conveyorScript;

    [SerializeField] public int score;

    // UI
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI lvlUpScoreText;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private TextMeshProUGUI startText;
    [SerializeField] private GameObject PauseUI;
    [SerializeField] private TextMeshProUGUI currentLVLtext;
    [SerializeField] private TextMeshProUGUI finalScoreText;

    private bool canUpdateUI;
    [SerializeField] private GameObject endGameUI;

    // Game
    public bool gameHasStarted;
    public bool gameIsPaused;
    
    private float timeRemaning;

    [SerializeField] private float levelTimer = 10f;
    [SerializeField] private int requiredScore = 200;

    public Transform spawnPoint;
    public Transform[] stopPositions;
    [SerializeField] public int nbDifficulty = 2;

    [SerializeField] private List<TrashItemData> failedItems;

    public GameObject[] Objects;

    // Sound
    [SerializeField] private AudioSource AS;
    [SerializeField] private AudioClip success;
    [SerializeField] private AudioClip failure;
    [SerializeField] private AudioClip other;

    private void Awake()
    {
        if (GMinstance == null)
        {
            GMinstance = this;
        }
        else
        {
            Destroy(gameObject); // Ensure there's only one instance
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // S'assurer que l'écran de démarrage est visible au début
        if (startScreen != null)
        {
            startScreen.SetActive(true);
        }

        //instances
        pickupScript = player.GetComponent<PickupAndThrow>();



        // UI
        scoreText.gameObject.SetActive(false);
        timeText.gameObject.SetActive(false);
        startText.gameObject.SetActive(true);
        lvlUpScoreText.gameObject.SetActive(false);
        endGameUI.SetActive(false);

        //Coroutines
        StartCoroutine(SpawnObjectsAdaptively());

        //The player cant move at the start
        pickupScript.ChangeHolding();
        player.GetComponent<BaseFirstPersonController>().pause = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (canUpdateUI && gameHasStarted)
        {
            //UI
            UpdateUI();


            // Time
            UpdateTime();


            // lvlUpscore
            UpdatelvlUpScore();


        }

        //Inputs
        InputCheck();
       
    }

    private void InputCheck()
    {
        if (!gameIsPaused)
        {
            //Show the pregame UI if the player press space while not already in game
            if (Input.GetKeyDown(KeyCode.Space) && gameHasStarted == false)
            {

                SetGameUI();
            }
        }
        

        //PauseInput
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            PauseGame();
        }
    }

    private void UpdateTime()
    {
        if(!gameIsPaused)
        {
            timeRemaning -= Time.deltaTime;
        }
        
        if (timeRemaning <= 0)
        {
            EndGame();
        }
    }

    private void UpdatelvlUpScore()
    {
        if (score >= requiredScore)
        {
            lvlUpScoreText.faceColor = Color.green;
        }
        else
        {
            lvlUpScoreText.faceColor = Color.white;
        }
    }

    public void UpdateScore(int amount)
    {

        score += amount;
        if (score <= 0)
        {
            score = 0;
        }
    }

    private void UpdateUI()
    {
        scoreText.text = score.ToString();
        timeText.text = timeRemaning.ToString("F0");
        lvlUpScoreText.text = requiredScore + " to lvl up";
    }

    public void PauseGame()
    {
        player.GetComponent<BaseFirstPersonController>().pause = !player.GetComponent<BaseFirstPersonController>().pause;
        gameIsPaused = !gameIsPaused;
        LockOrUnlockMouse();
        PauseUI.SetActive(!PauseUI.activeSelf);
        currentLVLtext.text = "Niveau "+ (nbDifficulty - 1).ToString();

        
    }
    public void addFailedItem(TrashItemData item)
    {
        failedItems.Add(item);
    }

    public bool isInFailedItem(TrashItemData item)
    {
        if (failedItems.Find(x => x == item))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

   

    public void LockOrUnlockMouse()
    {

        player.GetComponent<MouseLook>().SetCursorLock(!player.GetComponent<MouseLook>().lockCursor); 

    }

    void ChangeDifficulty(int newLevel)
    {
        AdjustSpawnRateAndRequiredScore(newLevel);
    }

    public int GetScore()
    {
        return score;
    }

    public int GetDifficultyLevel()
    {
        return nbDifficulty;
    }

    void CheckScore()
    {
        int checkFactor = 3;
        if (score >= requiredScore && score < requiredScore * checkFactor)
        {
            ChangeDifficulty(2);
        }
        else if (score >= requiredScore * checkFactor)
        {
            ChangeDifficulty(3);
        }
        else if (score < requiredScore)
        {
            ChangeDifficulty(1);
        }
    }

    IEnumerator SpawnObjectsAdaptively()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f); // Adjusts spawn rate based on difficulty

            GameObject[] objectsArray = Objects;

            if (Conveyor.objectsOnConveyor.Count < 5)
            {
                int randomIndex = Random.Range(0, objectsArray.Length);
                GameObject newObj = Instantiate(objectsArray[randomIndex], spawnPoint.position, Quaternion.identity);
                if (newObj.name.Contains("Bottle_Sebastien"))
                {
                    newObj.transform.rotation = Quaternion.Euler(-90, 0, 0);
                }
                newObj.GetComponent<BoxCollider>().isTrigger = true;
                conveyorScript.AddObjectToConveyor(newObj);
            }
        }
    }

    void AdjustSpawnRateAndRequiredScore(int lvl)
    {
        float upFactor = 1.5f;
        float downFactor = 0.65f;
        switch (lvl)
        {
            case 1: // Réduction
                requiredScore = (int)(requiredScore * downFactor);
                if( nbDifficulty > 1)
                {
                    nbDifficulty -= 1;
                }
                else
                {
                    nbDifficulty = 1;
                }
                
                break;
            case 2: // Petite augmentation
                requiredScore = (int)(requiredScore * upFactor);
                nbDifficulty += 1;
                break;
            case 3: // Grosse augmentation
                requiredScore = (int)(requiredScore * Mathf.Pow(upFactor, 2));
                nbDifficulty += 2;
                break;
        }
    }

    public void StartGame()
    {
        
        timeRemaning = levelTimer;
        scoreText.gameObject.SetActive(true);
        timeText.gameObject.SetActive(true);
        startText.gameObject.SetActive(false);
        lvlUpScoreText.gameObject.SetActive(true);
        canUpdateUI = true;
        gameHasStarted = true;
        failedItems.Clear();

        if (startScreen != null)
        {
            startScreen.SetActive(false);
        }

        
        

        
    }

    public void EndGame()
    {
        endGameUI.SetActive(true);
        ShowMistakes();
        scoreText.gameObject.SetActive(false);
        timeText.gameObject.SetActive(false);
        lvlUpScoreText.gameObject.SetActive(false);
        canUpdateUI = false;
        gameHasStarted = false;

        //player shouldnt move or pick an item
        if (pickupScript.GetHolding())
        {
            pickupScript.ChangeHolding();
        }
        player.GetComponent<BaseFirstPersonController>().pause = true;
        
        CheckScore();
    }

    private void ShowMistakes()
    {
        List<TextMeshProUGUI> children = new List<TextMeshProUGUI>(); 
        for (int i = 0; i< endGameUI.gameObject.GetComponentsInChildren<TextMeshProUGUI>().Length; i++)
        {
            if (endGameUI.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[i].transform.parent.gameObject.CompareTag("UiEndGame"))
            {
                children.Add(endGameUI.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[i]);
            } 
        }
        Debug.Log(children.Count);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        for (int i = 0; i < children.Count; i++)
        {
            children[i].gameObject.transform.parent.gameObject.SetActive(true);

            if (i < failedItems.Count)
            {
                children[i].GetComponentInChildren<TextMeshProUGUI>().text = failedItems[i].message;
            }
            else
            {
                children[i].gameObject.SetActive(false);
            }
        }
        finalScoreText.text = ( "Ton score final: "+ score + " X niveau "+nbDifficulty+" = "+ nbDifficulty*score).ToString();
    }

    public void SetGameUI()
    {
        endGameUI.SetActive(false);
        startScreen.SetActive(false);
        startText.gameObject.SetActive(true);

        //player should move and can pick an item
        pickupScript.ChangeHolding();
        player.GetComponent<BaseFirstPersonController>().pause = false;
        
        ResetObjects();
        if (pickupScript.GetHolding())
        {
            pickupScript.ChangeHolding();
        }
        

    }

    public void ResetObjects()
    {
        // Réinitialiser la position du joueur
        player.transform.position = new Vector3(2, 0, 6); 

        // Détruire l'objet dans la main du joueur s'il y en a un
        
        if (pickupScript != null && pickupScript.heldObject != null)
        {
            Destroy(pickupScript.heldObject);
            pickupScript.heldObject = null;
            pickupScript.ChangeHolding();
        }

        // Réinitialiser le convoyeur
        conveyorScript.ResetConveyor();
    }

    public void playSound(int sound)
    {
        switch (sound)
        {
            case 0:
                AS.clip = failure;
                AS.Play();
                break;
            case 1:
                AS.clip = success;
                AS.Play();
                break;
            case 2:
                AS.clip = other;
                AS.Play();
                break;
        }
    }

    
}
