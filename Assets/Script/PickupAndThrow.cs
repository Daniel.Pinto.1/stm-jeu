using System.Collections;
using UnityEditor;
using UnityEngine;




public class PickupAndThrow : MonoBehaviour
{
    public float throwForce = 6000;
    private Transform cameraTransform;
    private float holdDistance = 0.65f;
    private float maxObjectSize = 0.2f;
    public GameObject heldObject;
    private bool isHolding = false;
    public bool canPickUp = true;
    
    [SerializeField]private bool throwable = true;
    private Vector3 originalScale;
    private int originalLayer; // Pour stocker la layer originale de l'objet

    
    private float horizontalOffset = 0.29f; // D�calage horizontal vers la droite
    private float verticalOffset = -0.11f; // D�calage vertical vers le bas


    public Conveyor conveyorScript;
    public GameManager GM;

  

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        TrashItem.GoBackToHand += ReturnObjectToPlayer;
        TrashItem.DestroyObjectGameobject += ChangeHolding;

    }
    void Update()
    {
        if (Input.GetButtonDown("Fire1") )
        {
            if (!isHolding)
            {
                TryPickupObject();
            }
            else
            {
                if(throwable)
                {
                    ThrowObject();
                }
                
            }
        }

        if (isHolding && heldObject != null)
        {
            // Calcule la nouvelle position en ajoutant un d�calage en bas � droite
            Vector3 newPosition = cameraTransform.position
                                  + cameraTransform.forward * holdDistance
                                  + cameraTransform.right * horizontalOffset
                                  + cameraTransform.up * verticalOffset;

            heldObject.transform.position = newPosition;
        }
    }

    void TryPickupObject()
    {
        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, 2.5f))
        {
            if (hit.collider.gameObject.CompareTag("PickupObject") )
            {
                PickupObject(hit.collider.gameObject);
            }
        }
    }

    void PickupObject(GameObject pickObj)
    {
        Rigidbody objRig = pickObj.GetComponent<Rigidbody>();
        if(GM.gameHasStarted == false)
        {
            GM.StartGame();
        }
        if (objRig != null)
        {
            pickObj.GetComponent<BoxCollider>().isTrigger = false ;
            if (!isHolding)
            {
                originalScale = pickObj.transform.localScale; 
            }
            
            originalLayer = pickObj.layer; 
            pickObj.layer = LayerMask.NameToLayer("HeldObject"); // Change la layer pour �viter les collisions

            objRig.isKinematic = true;
            objRig.useGravity = false;
            AdjustObjectSize(pickObj);
            heldObject = pickObj;
            isHolding = true;

            
            if (conveyorScript != null)
            {
                conveyorScript.ObjectPickedUp(pickObj);
            }
            else
            {
                Debug.Log("R�f�rence au script Conveyor manquante dans PickupAndThrow");
            }
        }
    }



    public bool CanPickUp()
    {
        if (isHolding) { return false;  }
        else { return true; }
    }

    void ThrowObject()
    {
        Rigidbody objRig = null;

        if (heldObject != null)
        {
            objRig = heldObject.GetComponent<Rigidbody>();
            Debug.Log("testThrowObj");
        }
        
        if (objRig != null)
        {
            heldObject.transform.localScale = originalScale;
            heldObject.layer = originalLayer; // Restaure la layer originale

            heldObject.transform.parent = null;
            heldObject.GetComponent<TrashItem>().ChangeThrownStat();
            objRig.isKinematic = false;
            objRig.useGravity = true;
            objRig.AddForce(cameraTransform.forward * throwForce);

            heldObject = null;





        }
    }

    

    void AdjustObjectSize(GameObject obj)
    {
        float maxSize = Mathf.Max(obj.transform.localScale.x, obj.transform.localScale.y, obj.transform.localScale.z);
        if (maxSize > maxObjectSize)
        {
            float scaleFactor = maxObjectSize / maxSize;
            obj.transform.localScale = originalScale * scaleFactor;
        }
    }

    public void ReturnObjectToPlayer(TrashItem obj)
    {
        if (obj != null)
        {
            PickupObject(obj.gameObject);
            
            obj.ChangeThrownStat(); 
        }
    }

    public void ChangeHolding()
    {
        isHolding = !isHolding;
        
    }

    public bool GetHolding()
    {
        return isHolding;
    }

    
}